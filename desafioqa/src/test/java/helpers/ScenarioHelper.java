package helpers;

import org.openqa.selenium.WebDriver;

public class ScenarioHelper {

	private String homePage = "http://provaqa.marketpay.com.br:9081/desafioqa/inicio";
	private String loginPage = "http://provaqa.marketpay.com.br:9081/desafioqa";
	private WebDriver navegador;

	public ScenarioHelper(WebDriver navegador) {
		this.navegador = navegador;
	}

	public void visitHome() {
		navegador.get(this.homePage);
	}

	public void visitLoginPage() {
		navegador.get(this.loginPage);
	}

}
