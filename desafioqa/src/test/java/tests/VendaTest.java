package tests;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import helpers.ScenarioHelper;
import helpers.Web;
import pageObjects.LoginFormPage;

public class VendaTest {

	private WebDriver navegador;
	private ScenarioHelper scenarioHelper;
	private LoginFormPage loginFormPage;

	@Before
	public void setUp() {
		navegador = Web.createChrome();
		scenarioHelper = new ScenarioHelper(navegador);
		loginFormPage = new LoginFormPage(navegador);
		scenarioHelper.visitLoginPage();
	}

	@Test	
	public void testIncluirVenda() {
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirTransacao()
				.IncluirVenda("Davidzaque Araujo Leal", Float.parseFloat("20.00"));
		
		String sucessMessage = navegador
				.findElement(By.cssSelector("div[id='alertMessage'][class=' alert alert-success alert-dismissable']"))
				.getText();

		assertThat(sucessMessage, containsString("Venda realizada com sucesso"));
	}

	@After
	public void end() {
		navegador.quit();
	}
}
