package tests;

import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.*;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import helpers.ScenarioHelper;
import helpers.Web;
import pageObjects.LoginFormPage;

public class ClientTest {

	private WebDriver navegador;
	private ScenarioHelper scenarioHelper;
	private LoginFormPage loginFormPage;

	@Before
	public void setUp() {
		navegador = Web.createChrome();
		scenarioHelper = new ScenarioHelper(navegador);
		loginFormPage = new LoginFormPage(navegador);
		scenarioHelper.visitLoginPage();
	}

	@Test
	public void testAddCliente() {
		// CT006 – Incluir um Cliente
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirCliente()
				.fillCliente("Davidzaque Araujo Leal", "09716977441", true, Float.parseFloat("999.99"));

		String sucessMessage = navegador
				.findElement(By.cssSelector("div[id='alertMessage'][class=' alert alert-success alert-dismissable']"))
				.getText();

		assertThat(sucessMessage, containsString("Cliente salvo com sucesso"));
	}

	@Test
	public void testAddClienteWithoutFilling() {
		// CT007 – Incluir um Cliente sem preencher os campos.
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirCliente().clickSalvar();

		String errorMessage = navegador
				.findElement(By.cssSelector("small[class='help-block'][data-bv-for='nome'][data-bv-result='INVALID']"))
				.getText();

		assertThat(errorMessage, containsString("Campo Obrigatório"));

	}

	@Test
	public void testAddClienteFillingOnlyNome() {
		// CT008 – Incluir um Cliente preenchendo apenas o campo de Nome.
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirCliente().fillNome("Davidzaque Araujo")
				.clickSalvar();

		String errorMessage = navegador
				.findElement(By.cssSelector("small[class='help-block'][data-bv-for='cpf'][data-bv-result='INVALID']"))
				.getText();

		assertThat(errorMessage, containsString("Campo Obrigatório"));
	}

	@Test
	public void testAddClienteFillingOnlyCpf() {
		// CT009 – Incluir um Cliente preenchendo apenas o campo de CPF
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirCliente().fillCpf("09716977441")
				.clickSalvar();

		String errorMessage = navegador
				.findElement(By.cssSelector("small[class='help-block'][data-bv-for='nome'][data-bv-result='INVALID']"))
				.getText();

		assertThat(errorMessage, containsString("Campo Obrigatório"));
	}

	@Test
	public void testAddClienteFillingOnlySaldoBancario() {
		// CT010 – Incluir um Cliente preenchendo apenas o campo do Saldo Disponível.
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirCliente()
				.fillSaldoDisponivel(Float.parseFloat("500.50")).clickSalvar();

		String errorMessage = navegador
				.findElement(By.cssSelector("small[class='help-block'][data-bv-for='nome'][data-bv-result='INVALID']"))
				.getText();

		assertThat(errorMessage, containsString("Campo Obrigatório"));
	}

	@Test
	public void testAddClienteFillingOnlyNomeCpf() {
		// CT011 – Incluir um Cliente preenchendo apenas os campos de Nome e CPF.
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirCliente().fillNome("Raissa")
				.fillCpf("09088093440");

		String errorMessage = navegador
				.findElement(By
						.cssSelector("small[class='help-block'][data-bv-for='saldoCliente'][data-bv-result='INVALID']"))
				.getText();

		assertThat(errorMessage, containsString("Campo Obrigatório"));
	}

	@Test
	public void testAddClienteFillingOnlyNomeSaldoDisponivel() {
		// CT011 – Incluir um Cliente preenchendo apenas os campos de Nome e CPF.
		loginFormPage.loginFillingUserPassword("admin", "admin").incluirCliente().fillNome("Teste")
				.fillSaldoDisponivel(Float.parseFloat("500.50")).clickSalvar();

		String errorMessage = navegador
				.findElement(By.cssSelector("small[class='help-block'][data-bv-for='cpf'][data-bv-result='INVALID']"))
				.getText();

		assertThat(errorMessage, containsString("Campo Obrigatório"));
	}

	@Test
	public void testListarCliente() {
		// CT011 – Incluir um Cliente preenchendo apenas os campos de Nome e CPF.
		loginFormPage.loginFillingUserPassword("admin", "admin").clickListarCliente().listaCliente("Davidzaque") ;
		
		String primeiroCliente = navegador.findElements(By.cssSelector(".success>td")).get(0).getText();
		
		assertThat(primeiroCliente, containsString("Davidzaque"));
	}
	
	@After
	public void end() {
		//navegador.quit();
	}
}
