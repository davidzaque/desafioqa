package tests;

import static org.junit.Assert.*;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import helpers.ScenarioHelper;
import helpers.Web;
import pageObjects.LoginFormPage;

public class LoginTest {

	private WebDriver navegador;
	private ScenarioHelper scenarioHelper;
	private LoginFormPage loginFormPage;

	@Before
	public void setUp() {
		navegador = Web.createChrome();
		scenarioHelper = new ScenarioHelper(navegador);
		loginFormPage = new LoginFormPage(navegador);
		scenarioHelper.visitLoginPage();
	}

	@Test
	public void testLoginWithoutFilling() {
		// CT001 – Realizar o acesso com os campos vazios.
		loginFormPage.loginWithoutFilling();
		String errorMessage = loginFormPage.getErrorMessage();

		assertEquals(errorMessage, "Credenciais inválidas");
	}

	@Test
	public void testLoginFillingOnlyUser() {
		// CT002 – Realizar o acesso, preenchendo apenas o usuário.
		loginFormPage.loginFillingOnlyUser("admin");
		String errorMessage = loginFormPage.getErrorMessage();

		assertEquals(errorMessage, "Credenciais inválidas");
	}

	@Test
	public void testLoginFillingOnlyPassword() {
		// CT003 – Realizar o acesso, preenchendo apenas a senha.
		loginFormPage.loginFillingOnlyPassword("admin");
		String errorMessage = loginFormPage.getErrorMessage();

		assertEquals(errorMessage, "Credenciais inválidas");
	}

	@Test
	public void testLoginFillingUserAndWrongPassword() {
		// CT004 – Realizar o acesso, preenchendo com um usuário válido e uma senha
		// inválida
		loginFormPage.loginFillingUserPassword("admin", "admn");
		String errorMessage = loginFormPage.getErrorMessage();

		assertEquals(errorMessage, "Credenciais inválidas");
	}

	@Test
	public void testLoginFillingUserPassword() {
		// CT005 – Realizar o acesso, preenchendo com um usuário e senha válidos
		loginFormPage.loginFillingUserPassword("admin", "admin");
		String sucessMessage = navegador.findElement(By.className("page-title")).getText();

		assertEquals(sucessMessage, "Bem vindo ao Desafio");
	}

	@Test
	public void testLoginFillingWrondUser() {
		// CT024 – Realizar o acesso, preenchendo com um usuário inválido e uma senha
		// qualquer
		loginFormPage.loginFillingUserPassword("admn", "admn");
		String errorMessage = loginFormPage.getErrorMessage();

		assertEquals(errorMessage, "Credenciais inválidas");
	}

	@After
	public void end() {
		// navegador.quit();
	}
}
