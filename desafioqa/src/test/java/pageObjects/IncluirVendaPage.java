package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class IncluirVendaPage extends BasePage {

	public IncluirVendaPage(WebDriver navegador) {
		super(navegador);
	}

	public VisualizarVendaPage clickSalvar() {
		navegador.findElement(By.id("botaoSalvar")).click();

		return new VisualizarVendaPage(navegador);
	}

	public IncluirVendaPage selectCliente(String nome) {
		WebElement campoClientes = navegador.findElement(By.id("cliente"));
		new Select(campoClientes).selectByVisibleText(nome);

		return this;
	}

	public IncluirVendaPage fillValorTransacao(Float valor) {
		JavascriptExecutor js = (JavascriptExecutor) navegador;
		js.executeScript("document.getElementById('valorTransacao').setAttribute('value','" + valor + "')");

		return this;
	}

	public VisualizarVendaPage IncluirVenda(String nomeCliente, Float valorTransacao) {
		selectCliente(nomeCliente);
		fillValorTransacao(valorTransacao);
		clickSalvar();

		return new VisualizarVendaPage(navegador);
	}

	public ListarVendaPage clickCancelar() {
		navegador.findElement(By.linkText(" Cancelar ")).click();

		return new ListarVendaPage(navegador);
	}
}
