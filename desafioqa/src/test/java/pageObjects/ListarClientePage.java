package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ListarClientePage extends BasePage {

	public ListarClientePage(WebDriver navegador) {
		super(navegador);
	}

	public ListarClientePage fillNome(String nome) {
		navegador.findElement(By.cssSelector("input[name='j_idt17']")).sendKeys(nome);

		return this;
	}

	public ListarClientePage clickPesquisar() {
		navegador.findElement(By.cssSelector("input[name='j_idt20'][value='Pesquisar']")).click();

		return this;
	}

	public ListarClientePage clickLimparBase() {
		navegador.findElement(By.cssSelector("input[name='j_idt22'][value='Limpar Base']")).click();

		return this;
	}

	public ListarClientePage listaCliente(String nome) {
		fillNome(nome).clickPesquisar();

		return this;
	}

}
