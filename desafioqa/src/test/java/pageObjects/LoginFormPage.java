package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginFormPage extends BasePage {

	public LoginFormPage(WebDriver navegador) {
		super(navegador);
	}

	public String getErrorMessage() {
		return navegador.findElement(By.cssSelector("font[color=\"red\"]>label")).getText();
	}

	public LoginFormPage typeUser(String user) {
		navegador.findElement(By.cssSelector("input[name=\"username\"]")).sendKeys(user);

		return this;
	}

	public LoginFormPage typePassword(String password) {
		navegador.findElement(By.cssSelector("input[name=\"password\"]")).sendKeys(password);

		return this;
	}

	public HomePage clickSigIn() {
		navegador.findElement(By.cssSelector("button[type=\"submit\"][class=\"btn btn-primary\"]")).click();

		return new HomePage(navegador);
	}

	public HomePage loginWithoutFilling() {
		clickSigIn();

		return new HomePage(navegador);
	}

	public HomePage loginFillingOnlyUser(String user) {
		typeUser(user);
		clickSigIn();

		return new HomePage(navegador);
	}

	public HomePage loginFillingOnlyPassword(String password) {
		typePassword(password);
		clickSigIn();

		return new HomePage(navegador);
	}

	public HomePage loginFillingUserPassword(String user, String password) {
		typeUser(user);
		typePassword(password);
		clickSigIn();

		return new HomePage(navegador);
	}
}
