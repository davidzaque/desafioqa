package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class IncluirClientePage extends BasePage {

	
	public IncluirClientePage(WebDriver navegador) {
		super(navegador);
	}

	public IncluirClientePage fillNome(String nome) {
		navegador.findElement(By.id("nome")).sendKeys(nome);

		return this;
	}

	public IncluirClientePage fillCpf(String cpf) {
		JavascriptExecutor js = (JavascriptExecutor) navegador;
		js.executeScript("document.getElementById('cpf').setAttribute('value','" + cpf + "')");

		return this;
	}

	public IncluirClientePage isAtivo(Boolean isAtivo) {
		navegador.findElement(By.cssSelector("#status>option[value='" + isAtivo + "']")).click();

		return this;
	}

	public IncluirClientePage fillSaldoDisponivel(Float saldoBancario) {
		JavascriptExecutor js = (JavascriptExecutor) navegador;
		js.executeScript("document.getElementById('saldoCliente').setAttribute('value','" + saldoBancario + "')");
		// navegador.findElement(By.id("saldoCliente")).sendKeys(saldoBancario);

		return this;
	}

	public IncluirClientePage clickClearAllFields() {
		navegador.findElement(By.id("botaoLimpar")).click();

		return this;
	}

	public IncluirClientePage clickCancel() {
		navegador.findElement(By.cssSelector("a[href=\"/desafioqa/listarCliente\"]"));

		return this;
	}

	public IncluirClientePage clickSalvar() {
		navegador.findElement(By.id("botaoSalvar")).click();

		return this;
	}

	public IncluirClientePage fillCliente(String nome, String cpf, Boolean isAtivo, Float saldoBancario) {
		fillNome(nome).fillCpf(cpf).isAtivo(isAtivo).fillSaldoDisponivel(saldoBancario).clickSalvar();

		return this;
	}
}
