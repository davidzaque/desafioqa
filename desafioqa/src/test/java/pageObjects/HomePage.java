package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {

	public HomePage(WebDriver navegador) {
		super(navegador);
	}

	public HomePage clickInicio() {
		navegador.findElement(By.cssSelector("a[href='/desafioqa/inicio']")).click();

		return this;
	}

	public LoginFormPage clickSair() {
		navegador.findElement(By.cssSelector("input[value='SAIR']")).click();

		return new LoginFormPage(navegador);
	}

	public HomePage clickMenuQA() {
		navegador.findElement(By.cssSelector("a[title='QA']")).click();

		return this;
	}

	public HomePage clickClientes() {
		navegador.findElement(By.cssSelector("a[title='Clientes']")).click();

		return this;
	}

	public IncluirClientePage clickIncluirCliente() {
		navegador.findElement(By.cssSelector("a[href='/desafioqa/incluirCliente'")).click();

		return new IncluirClientePage(navegador);
	}

	public IncluirClientePage incluirCliente() {
		clickMenuQA().clickClientes().clickIncluirCliente();

		return new IncluirClientePage(navegador);
	}

	public ListarClientePage clickListar() {
		navegador.findElement(By.cssSelector("a[href='/desafioqa/listarCliente']")).click();

		return new ListarClientePage(navegador);
	}

	public ListarClientePage clickListarCliente() {
		clickMenuQA().clickClientes().clickListar();

		return new ListarClientePage(navegador);
	}
	
	public HomePage clickTransacao() {
		navegador.findElement(By.cssSelector("a[title='Transações']")).click();
		
		return this;
	}

	public IncluirVendaPage clickIncluirTransacao() {
		navegador.findElement(By.cssSelector("a[href='/desafioqa/incluirVenda']")).click();

		return new IncluirVendaPage(navegador);
	}
	
	public IncluirVendaPage incluirTransacao() {
		clickMenuQA().clickTransacao().clickIncluirTransacao();

		return new IncluirVendaPage(navegador);
	}

	public ListarVendaPage clickListarTransacao() {
		navegador.findElement(By.cssSelector("a[href='/desafioqa/listarVenda']")).click();

		return new ListarVendaPage(navegador);
	}

}
